import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="vsnap",
    version="0.0.1",
    author="Punita Repe",
    author_email="pdr8@njit.edu",
    description="Vsnap client",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/punita01/vsnap/",
    packages=setuptools.find_packages(),
    classifiers=(
        "Programming Language :: Python :: 3.4",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ),
)
